---
stage: Manage
group: Workspace
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Group access and permissions

Use Groups to manage one or more related projects at the same time.

For instructions on how to configure access and permissions for groups, see [Groups](index.md).
